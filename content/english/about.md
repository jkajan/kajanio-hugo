---
title: "Hi."
date: 2020-08-30T17:40:13+03:00
draft: false
---

My name is Jonatan Kaján, and I live in Helsinki. I am currently a fifth year student at [Aalto University](https://aalto.fi), pursuing a degree in Electronics and Electrical Engineering. I work at [Eficode](https://eficode.com), where I mostly do things related to monitoring of web services. I also spend a lot of time at [Teknologföreningen](https://tf.fi), where I am active in many different areas.

In my spare time I dabble in coding, music making, and electronics.

# Links

* [Gitlab](https://gitlab.com/jkajan/)
* [Github](https://github.com/jkajan/)
