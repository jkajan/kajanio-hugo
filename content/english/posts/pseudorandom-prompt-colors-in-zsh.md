---
title: "Pseudorandom Prompt Colors in zsh"
date: 2021-05-05T11:16:35+03:00
draft: false
tags: [base16, zsh, bash, ricing]
---
I'm an avid user of terminal-based programs both on my local machines and
the throng of servers i regularly `ssh` into. To keep track of which machine I'm
currently tinkering with I have the current hostname printed on my terminal
prompt. Even though it's literally next to what I'm writing I still sometimes
manage to run things on the wrong host.

To make the terminals a little bit easier to tell apart, but not mess with my
current colour scheme, I wrote a `zsh` function to set the colour of the
hostname pseudorandomly.

```bash
#.zshrc
#======
# ---- snip ----
# Generate hostname with pseudorandom color.
# Offsets color rotation with $1 steps generates
# RNG seed using $HOST+$2.
function prc_hostname {
	RANDOM=$(echo $HOST$2 | cksum | tr -d " ")
	OFFSET=$1
	echo '%F{'$(( 1 + (RANDOM + OFFSET) % 6 ))'}%m%f'
}

PROMPT='$(hostname_clr) %% '
```

The function `hostname_clr` will print the hostname with accompanying tags to
change the color. Setting the variable `$RANDOM` sets the seed of the RNG.
Here I set it to the checksum of the hostname (plus the second
argument of the function, which is optional).

The expression `$(( 1 + (RANDOM + OFFSET) % 6 ))` will produce a number between
1 and 6, which corresponds to the colors red, green, yellow, blue, magenta, and
cyan (depending on the theme). Since the seed is based on the hostname, the
color will stay the same from session to session.
