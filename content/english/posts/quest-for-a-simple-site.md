---
title: "The quest for a simple site"
date: 2020-09-06
draft: false
categories: ["personal website"]
tags: [ansible, hugo, base16]
---

A couple of years ago, I started getting interested in web development. I had a defunct HTPC lying around that I didn't have any plans for and thought, why not host a website on it? The hardware wasn't enough for running some sort of game server, it didn't have a GPU that would have done much running Folding@Home or something similar, but some simple, single serving site seemed perfect.

After buying some cheap domains and playing around with HTML, CSS, Apache for a while, it felt like I started to get the hang of things. One thing that I wanted to do was to create some sort of personal website, like all the other cool kids were doing. I bought [kajan.io](https://kajan.io), and continued my adventures there.

Rather quickly getting enough of editing HTML by hand, I started looking into content management systems. The only one I really knew of was Wordpress, so that was my first choice. Not finding any themes that I really liked, I made [my own](https://github.com/jkajan/wp-kajan) instead. Since Wordpress is focused on blogging I even started writing a blog of sorts, but quickly realised that it didn't really interest me that much. Since running a full Wordpress instance for a very simple site with just a handful of pages felt like overkill, I started looking into alternatives.

After a brief stint of using [Grav](https://getgrav.org), I felt like I wanted to try something new once again. This site that you are currently viewing is now built with [Hugo](https://gohugo.io), built from a repo on [Gitlab](https://gitlab.com/jkajan/kajanio-hugo), and hosted on a cute little VPS from [Hetzner](https://hetzner.de). We'll see how long it takes before I decide to rework the site completely once again, but for the time being I'm happy with the result.
