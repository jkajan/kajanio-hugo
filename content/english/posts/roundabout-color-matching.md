---
title: "Roundabout color matching"
date: 2021-01-14T13:47:33+02:00
draft: false
categories: ["personal website"]
tags: [ansible, hugo, base16]
---

When doing computer work, both _at work_ and on other projects, I spend much of
my time in terminal emulators. My first choice of text editor is (in most
cases) `nvim`, and doing _DevOps_-related things leads to quite a lot of `ssh`,
`git`, `ansible`, and so on. Since I also use `bspwm` (a tiling window manager)
on all my computers, doing things in terminals is often the most
straightforward way. _The key word here being "often". I'm actually not enough
of a l33t hackerman that I use `lynx` for web browsing._

Using mostly TUIs means I'm also rather particular about the looks of my
terminal. For several years I've been a big fan of [chriskempsons' _base16_
theme architecture](https://github.com/chriskempson/base16), and gone to [great
lengths](https://gitlab.com/jkajan/dotfiles/-/blob/master/dot_config/wal/executable_postwal.sh)
to use the same color scheme in as many places as possible. Since I'm deadly
serious about my _"ricing"_, this website is no exception.

The included command to deploy a `hugo`-generated website only
supports AWS, Azure, and the like, so I deploy mine using Ansible. I also
made a playbook to pull all `base16` colorschemes [listed on
Github](https://github.com/chriskempson/base16-schemes-source):

```yaml
---
# get_base_16_colors.yml
- name: Pull all base16 colorschemes
  hosts: kajan.io
  tasks:
    - block:
      - name: Create temp file
        ansible.builtin.tempfile:
          state: file
          suffix: _base16
        register: tempfile

      - name: Download list of colorscheme repos
        ansible.builtin.get_url:
          url: "{{ repo_yaml }}"
          dest: "{{ tempfile.path }}"
        vars:
          repo_yaml: https://raw.githubusercontent.com/chriskem\
                     pson/base16-schemes-source/master/list.yaml

      - name: Read URLs from file
        include_vars:
          file: "{{ tempfile.path }}"
          name: schemes
      delegate_to: localhost
      connection: local

    - name: Pull all colorschemes
      ansible.builtin.git:
        repo: "{{ item.value }}"
        dest: "{{ dir }}/{{ item.key }}"
        clone: yes
        version: master
        update: no
        depth: 1
      loop: "{{ schemes | dict2items }}"

    - name: Get yamls
      ansible.builtin.find:
        paths: files/base16-colorschemes
        recurse: yes
        depth: 2
        use_regex: yes
        patterns:
          - ".*\\.yaml"
      register: color_yamls

    - name: Copy all colorschemes to files
      ansible.builtin.copy:
        src: "{{ item.path }}"
        dest: "{{ dir }}/base16-{{ item.path | basename }}"
      loop: "{{ color_yamls.files }}"
```

After running this I had all the colorschemes I could ever want! Then it was
a easy to create my own custom CSS using `ansible.builtin.template` and
include it in the deployment task.

After spending way too long on automating
this (which perhaps really wasn't in need of automating), I realised that I
would still need to create a separate style for the [code block syntax
highlighting](https://gohugo.io/content-management/syntax-highlighting/). After
pondering this for a short while and realising that someone has probably done
this already, I found the [base16-hugo repo](https://github.com/yawpitch/base16-hugo)
that I should have used from the beginning, instead of reinventing the wheel.

It would seem that I still have some way to go before I'm happy with this website, but I'm at least partway there!
