---
title: "Public keys"
date: 2021-01-13T11:05:17+02:00
draft: false
---
My public keys currently in use are:

```ini
ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIEJOigpWhvjNXWDHRzLjksNwt2qDwr5MMq1nKo76qj//  jkajan@cronus
ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIMEHs18VsaQISTdIMYuQRc87lF/Npd+K2Dr0WVQSw3oL  jkajan@iapetus
ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIDXF5r9n3JxHSh/UT2dTdQXNDMjqrZ1AujQcjwBhDARO  jkajan@cronus_win
ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAICmoyFhEjmZxIhTFCY8+Fap9POGtZ3oVL+Oww/u/zqbf jkajan@telesto-master
```
