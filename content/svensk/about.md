---
title: "Hallåja."
date: 2020-08-30T17:40:13+03:00
draft: false
---

Jag heiter Jonatan Kaján och bour i Helsingfors. Om int ni redan kuna giss e basera på diftongan så e ja alltså från Österbotten från början.

Jag jir för stååndin en femt årets studerand ve [Aalto-universitete](https://aalto.fi), täär ja läser Elektronik och Elektroteknik. Jag jobbar ve [Eficode](https://eficode.com), täär ja mest sysslar me saker relatera ti monitoring ååv webtjänster å tåli.

På fritidin så jir ja rätt så mytchi ve [Teknologföreningen](https://tf.fi), täär ja e aktiv inom maang områden. I år så fungerar ja som _Datör_ å leider våran sysadminkomitté. Tutar också i tid och otid i [Humpsvakar](https://humps.fi). Förutom he så påtar ja nu en heil deil me servar, heimsiidor, musik, och allt möjlit anna.

# Länkar

* [Gitlab](https://gitlab.com/jkajan/)
* [Github](https://github.com/jkajan/)
