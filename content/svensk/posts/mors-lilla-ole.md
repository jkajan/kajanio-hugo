---
title: "Mors Lilla Olé"
date: 2021-05-05T18:14:05+03:00
draft: false
tags: [snapsvisor, musik]
---
Va jeer i vikon med på en workshop i snapsviseskrivande, som ordnadist i samband
med att vi förnyar TF:s sångbok inför TF150. Täär vi vi i uppdrag att skriv en
_sjuminutersvisa_ med tema _"Sol"_ å _"Mors lilla olle"_ som melodi.

Som inspiration tog ja en mörk men uppfinningsrik kväll i Otnäs för en tid sedan
när vi va snål på öl i mexikansk stil, men sakna både öl i mexikansk stil samt
färsk citron.
```
Mors lilla Olle så torr i trut
Cervezan va drucken, citronen va slut
I skåpet fanns endast en pilsner i plåt
Citronsaft på flaska;  Mexiko förlåt!
```
Takten skiter sig lite på sista raden, men int alltför dåligt prestera på sju
minuter!
