---
title: "Nöög ännu breidan än förr"
date: 2020-01-13
draft: false
categories: ["heimsiido"]
---
Jag har haft hanje iterationin ååv min heimsiido en ståånd nu, men int precis uppdatera an naa. Tyckt e va dags att pååt helst na liite me an, å hije kan ni läs slutresultate. Nu finns alltså heje literära mästerverke både på the Queen's English samt rå, oufiltrera _Replotdialekt_. Tide längst ner finns tillåme knappar å allt för ti byt språk; tåfft va?

Som e funkar i praktikin så finns e oulika artiklar på "svenska" å engelska versionin, eftersom ja ousannolikt kommer ha varken lust elo ork att översätt he ja råddar jer. He som kommer va på engelsk så e högst antagligen IT-relatera; kodprojekt och sånt, medan he som e på breidan språåk e meira persounliga grejor. Kanski texter ti snapsvisor å tåli, ifall ja räfsar ihop ti ja har å lagar dom färdi.
